# HFA-candidate-profile

_This project is used for testing purpose_

Welcome to this awesome project that we wish you will enjoy. It will require you to use all your knowledge and skills to build a prototype interface that will allow us to manage candidates on our platform.

## Objective(s):

- Based on the provided PDF document you are requested to build a prototype of this interface that will allow us to view and edit a candidate and also with the *Comments* panel to add some more details on a candidate when our recruiters are interviewing them.
- The prototype will need to be responsive of course.

## Details:

- Only the profile tab is visible, the others are disabled (no action is expected on them)
- The *Experiences* and *Formations* tabs are not editable but are showing only one element by default. When clicking on "show more..." it display the rest of the elements and the user can click on "show less..." to only display the default one.
- The *Current Professional Status*, *Wished Professional Status* and *Languages* blocks are editable as displayed on the 3rd screen.
- To allow the recruiter to have all the informations when writing comments, when the panel is displayed, the *Current Professional Status*, *Wished Professional Status* and *Languages* blocks are displayed after the *Experiences* and *Formations* ones.
- The left menu has a focus on the current domain explored (here "Candidates") when clicking on the others nothing happen.

## Expectation(s):

### Technical requirements:

- Work done using mainly HTML, CSS (or SASS) and ES6 (no jQuery)

### Results:

- Link to your work repository with a README indicating:
  - how to run it.
  - Feedbacks on the main problems met during the realization of the exercice.
  - The amount time passed on it.
  - Feedbacks on how to improve the project if we needed to make more appealing to the users.

Good Luck
